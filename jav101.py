#!/usr/bin/python
import os
import sys
import tempfile
from threading import Thread
import shutil
from Queue import Queue
from contextlib import contextmanager
import urllib2
from urlparse import urlparse
import json
import re
import requests
import m3u8
from progress.bar import Bar
from bs4 import BeautifulSoup

NUM_OF_THREADS = 5


def get_info(url):
    pattern = re.compile('var data = (.*?);')
    request = urllib2.Request(url)
    request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36')
    web = urllib2.urlopen(request)
    soup = BeautifulSoup(web.read(), 'lxml')
    scripts = soup.find_all('script')
    for script in scripts:
        if not script.string:
            continue
        if (pattern.match(script.string.encode('utf-8').strip())):
            data = pattern.match(script.string.encode('utf-8').strip())
            video = json.loads(data.groups()[0])['video']
            m3u8_url = video['specurl']['m3u8']
            if m3u8_url.startswith('//'):
                m3u8_url = 'http:{}'.format(m3u8_url)
            return {'name': video['name'], 'm3u8': m3u8_url}
    return None


def get_prefix(m3u8_url):
    url = urlparse(m3u8_url)
    return '{}://{}{}'.format(url.scheme,
                              url.netloc,
                              os.path.dirname(url.path))


def get_m3u8_segments(m3u8_url, prefix):
    r = requests.get(m3u8_url)
    m3u8_obj = m3u8.loads(r.text)
    return [os.path.join(prefix, segment.uri) for segment in m3u8_obj.segments]


@contextmanager
def mkdtemp():
    temp_dir = tempfile.mkdtemp(dir='.')
    yield temp_dir
    shutil.rmtree(temp_dir)


def download_segment(segments, referer, temp_dir, bar):
    while True:
        segment = segments.get()
        filename = os.path.join(temp_dir, os.path.basename(segment))
        r = requests.get(segment, stream=True, headers={'referer': referer})
        with open(filename, 'wb') as file:
            for chunk in r:
                file.write(chunk)
        bar.next()
        segments.task_done()


def slugify(value):
    return re.sub('[?><:"\\/|\*"]', '', value)

url = sys.argv[1]

info = get_info(url)
prefix = get_prefix(info['m3u8'])
segments = get_m3u8_segments(info['m3u8'], prefix)

filename = slugify('{}.ts'.format(info['name'].encode('utf-8')))
bar = Bar('Downloading...{}'.format(filename), max=len(segments))
queue = Queue()

with open(filename, 'wb') as destination, mkdtemp() as temp_dir:
    for i in range(NUM_OF_THREADS):
        thread = Thread(target=download_segment,
                        args=(queue, url, temp_dir, bar))
        thread.setDaemon(True)
        thread.start()

    for segment in segments:
        queue.put(segment)
    queue.join()
    bar.finish()

    for segment in segments:
        with open(os.path.join(temp_dir, os.path.basename(segment))) as f:
            shutil.copyfileobj(f, destination)
